# LFS Test

Git Large File Storage

1. Enable it in the repo: https://gitlab.oit.duke.edu/dpb6/lfs-test/edit > Permissions > Repository > Git Large File Storage
2. Read Directions: https://gitlab.oit.duke.edu/help/workflow/lfs/manage_large_binaries_with_git_lfs
3. Get the Git LFS client and install it: https://git-lfs.github.com
4. Go to repo in Git Shell and do: 
```bash
git lfs install
git lfs track "*.pptx"
git add .gitattributes
git add Bradway_Trahey_2018.pptx
git commit -m "add PowerPoint"
git push origin master
```